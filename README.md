# 聊天室

## 技术

1. Java 21
2. Springboot 3.2
3. Gradle
4. WebFlux
5. gRPC

## 简介

使用了spring boot 3.2.0 和 gRPC来实现一个聊天室。

## 时间线

### 2023-12-03

1. 结构   
   创建了一个spring boot 项目  
   分成了服务器端和客户端
2. 依赖
    * spring web
    * spring webflux
    * spring security
    * gRPC
    * lombok
    * JJwt
    * JPA
    * postgreSQL
